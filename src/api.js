"use strict";
require("dotenv").config();
const express = require("express");
let cors = require("cors");
const serverless = require("serverless-http");
const bodyParser = require("body-parser");
let admin = require("firebase-admin");

const categoriesRouter = require("./routes/categories");
const tasksRouter = require("./routes/tasks");

const app = express();
const router = express.Router();

const allowedOrigin = process.env.ALLOWED_ORIGIN_URL;

const corsOptions = {
  origin: function (origin, callback) {
    if (allowedOrigin === origin) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};

admin.initializeApp({
  credential: admin.credential.cert({
    client_email: process.env.FIREBASE_CLIENT_EMAIL,
    private_key: process.env.FIREBASE_PRIVATE_KEY,
    project_id: process.env.FIREBASE_PROJECT_ID,
  }),
  databaseURL: process.env.FIREBASE_DATABASE_URL,
});

router.use("/categories", categoriesRouter);
router.use("/tasks", tasksRouter);

router.get("/", (req, res) => {
  res.json({
    hello: "hi!",
  });
});

//app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use("/.netlify/functions/api", router);

module.exports.handler = serverless(app);
